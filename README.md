This is a dist-git like repo for [libntirpc](https://github.com/nfs-ganesha/libntirpc).

Note: nfs-ganesha-28 is the version of nfs-ganesha that corresponds to (lib)ntirpc-1.8, nfs-ganesha-30 corresponds to ntirpc-3.0, and so on.

The master brach is unused. Use an existing branch instead.
Branch names follow convention like `c<VERSION>-sig-storage-nfs-ganesha-<GANESHA-VERSION>` as descibed on [Naming and Patterns for Mapping Git Branches to Koji Tags](https://wiki.centos.org/BrianStinson/GitBranchesandKojiTags)

* c7-sig-storage-nfs-ganesha-28: CentOS-7, nfs-ganesha-28 and ntirpc-1.8
* c7-sig-storage-nfs-ganesha-30: CentOS-7, nfs-ganesha-30 and ntirpc-3.0
* ...

Instructions for building the libntirpc and nfs-ganesha packages for the CentOS Storage SIG can be found in the following places:

* [Community Build System](https://wiki.centos.org/HowTos/CommunityBuildSystem)
* [Storage SIG landing page](https://wiki.centos.org/SpecialInterestGroup/Storage/Gluster)

Build the src.rpm with:

    $ rpmbuild -bs \
               --define "_sourcedir $PWD/SOURCES" --define "_srcrpmdir $PWD" \
               --define "dist .el7" SPECS/libntirpc.spec

To build:

`cbs build [--scratch] storage7-nfsganesha-28-el7 libntirpc-1.8.1-1.src.rpm

